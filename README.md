# Mon premier dépôt Git
Ceci est mon premier dépôt.

## Liste des commandes
- 'git init' : initialise le dépôt
- 'git add' : ajoute un fichier à la zone d'index
- 'git commit' : valide les modifications indexées dans la zone d'index
- 'git status' : statut du dépôt
- 'git log' : affiche l'historique
- 'git remote'
- 'git push' : permet de pousser des commits vers un dépôt distant
- 'git clone [lien SSH ou HTTPS]' : permet de cloner un dépôt distant en local

